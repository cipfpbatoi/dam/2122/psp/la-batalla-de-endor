package ejercicio1;

import java.util.concurrent.ThreadLocalRandom;

public class XWing implements Runnable{

    String name;
    int intervalos;
    int velocidad;
    int distanciaTotal;

    public XWing(int intervalos, int velocidad, int distanciaTotal) {
        this.intervalos = intervalos;
        this.velocidad = velocidad;
        this.distanciaTotal = distanciaTotal;
    }

    public XWing(String name) {
        this(1000,500,20000);
        this.name = name;
    }

    private boolean matarUnCaza(){
        return (ThreadLocalRandom.current().nextInt(0,100)<= 80 );
    }

    @Override
    public void run() {

        Thread.currentThread().setName("XWing " + name);

        int cazasImperialesMuertos = 0;

        for (int distanciaRecorrida = velocidad; distanciaRecorrida<=distanciaTotal; distanciaRecorrida+=velocidad){

            try {
                Thread.sleep(intervalos);
            } catch (InterruptedException e) {
                System.out.println("El " + Thread.currentThread().getName() + " ha sido abatido");
                return;
            }

            boolean mataAunCaza = false;

            if (mataAunCaza = matarUnCaza()) {
                cazasImperialesMuertos++;
            }

            if ( mataAunCaza){
                System.out.println("El " + Thread.currentThread().getName() + " ha acabado con un caza y lleva recorridos " +
                        " " + distanciaRecorrida);
            } else {
                System.out.println("El " + Thread.currentThread().getName() + " no ha matado ningun caza y lleva recorridos " +
                        " " + distanciaRecorrida);
            }

        }

        System.out.println("El " + Thread.currentThread().getName() + " ha llegado a la Estrella de Muerte y ha derribado a " +
                "" + cazasImperialesMuertos + " cazas imperiales");

    }
}
