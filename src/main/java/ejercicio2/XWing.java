package ejercicio2;

import java.util.concurrent.ThreadLocalRandom;

public class XWing implements Runnable{

    String name;
    int intervalos;
    int velocidad;
    int distanciaTotal;
    Escuadron escuadron;

    public XWing(int intervalos, int velocidad, int distanciaTotal, Escuadron escuadron) {
        this.intervalos = intervalos;
        this.velocidad = velocidad;
        this.distanciaTotal = distanciaTotal;
        this.escuadron = escuadron;
    }

    public XWing(String name,Escuadron escuadron) {
        this(1000,500,20000,escuadron);
        this.name = name;
    }

    private boolean matarUnCaza(){
        return (ThreadLocalRandom.current().nextInt(0,100)<= 80 );
    }

    @Override
    public void run() {

        Thread.currentThread().setName("XWing " + name);

        int cazasImperialesMuertos = 0;

        for (int distanciaRecorrida = velocidad; distanciaRecorrida<=distanciaTotal; distanciaRecorrida+=velocidad){

            try {
                Thread.sleep(intervalos);
            } catch (InterruptedException e) {
                System.out.println("El " + Thread.currentThread().getName() + " ha sido abatido");
                return;
            }

            boolean mataAunCaza = false;

            if (mataAunCaza = matarUnCaza()) {
                cazasImperialesMuertos++;
                escuadron.incrementarCazasAbatidos();
            }

            if ( mataAunCaza){
                System.out.println("El " + Thread.currentThread().getName() + " ha acabado con un caza y lleva recorridos " +
                        " " + distanciaRecorrida);
            } else {
                System.out.println("El " + Thread.currentThread().getName() + " no ha matado ningun caza y lleva recorridos " +
                        " " + distanciaRecorrida);
            }

        }

        System.out.println("El " + Thread.currentThread().getName() + " ha llegado a la Estrella de Muerte y ha derribado a " +
                "" + cazasImperialesMuertos + " cazas imperiales");

    }
}
