import ejercicio1.XWing;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Ejercicio1 {

    public static void main(String[] args) {

        System.out.println("Empezando simulación");

        ExecutorService executorService = Executors.newFixedThreadPool(20);

        for (int i = 0; i < 10; i++) {

            executorService.execute(new XWing(i+""));

        }

        executorService.shutdown();

    }

}
