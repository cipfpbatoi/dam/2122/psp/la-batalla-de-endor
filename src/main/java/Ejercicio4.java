import ejercicio3.Escuadron;
import ejercicio4.EstrellaDeLaMuerte;
import ejercicio3.XWing;
import ejercicio4.CazaImperial;

public class Ejercicio4 {

    public static void main(String[] args) {

        int num_naves_rebeldes = 10;

        Escuadron escuadron = new Escuadron();

        XWing[] naves = new XWing[num_naves_rebeldes];
        Thread[] th_naves = new Thread[num_naves_rebeldes];

        EstrellaDeLaMuerte estrellaDeLaMuerte = new EstrellaDeLaMuerte(th_naves);
        Thread th_estrella = new Thread(estrellaDeLaMuerte);

        CazaImperial[] cazaImperials = new CazaImperial[10];


        for (int i = 0; i < num_naves_rebeldes ; i++) {
            naves[i] = new XWing(i+"",escuadron,th_estrella);
            th_naves[i] = new Thread(naves[i]);
        }

        for (int i = 0; i < 10 ; i++) {

           cazaImperials[i] = new CazaImperial(th_naves);
           cazaImperials[i].start();

        }

        for (Thread xwing: th_naves) {
            xwing.start();
        }

        th_estrella.start();

        for (Thread xwing: th_naves) {
            try {
                xwing.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("El escuadrón rebelde ha acabado con un total de " + escuadron.getNumeroCazasAbatidos() + " cazas");

        try {
            th_estrella.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 10 ; i++) {

            cazaImperials[i].interrupt();

        }


    }

}
