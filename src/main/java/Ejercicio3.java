import ejercicio3.Escuadron;
import ejercicio3.EstrellaDeLaMuerte;
import ejercicio3.XWing;

public class Ejercicio3 {

    public static void main(String[] args) {

        int num_naves_rebeldes = 10;

        Escuadron escuadron = new Escuadron();

        XWing[] naves = new XWing[num_naves_rebeldes];
        Thread[] th_naves = new Thread[num_naves_rebeldes];

        EstrellaDeLaMuerte estrellaDeLaMuerte = new EstrellaDeLaMuerte(th_naves);
        Thread th_estrella = new Thread(estrellaDeLaMuerte);

        for (int i = 0; i < num_naves_rebeldes ; i++) {
            naves[i] = new XWing(i+"",escuadron,th_estrella);
            th_naves[i] = new Thread(naves[i]);
        }



        for (Thread xwing: th_naves) {
            xwing.start();
        }

        th_estrella.start();

        for (Thread xwing: th_naves) {
            try {
                xwing.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("El escuadrón rebelde ha acabado con un total de " + escuadron.getNumeroCazasAbatidos() + " cazas");

        try {
            th_estrella.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
