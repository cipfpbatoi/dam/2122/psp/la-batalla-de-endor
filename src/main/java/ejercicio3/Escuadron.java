package ejercicio3;

public class Escuadron {

    private int numeroCazasAbatidos = 0;

    public synchronized void incrementarCazasAbatidos()
    {
        numeroCazasAbatidos++;
    }

    public synchronized int getNumeroCazasAbatidos()
    {
        return numeroCazasAbatidos;
    }

}
