package ejercicio4;

import java.util.concurrent.ThreadLocalRandom;

public class CazaImperial extends Thread{

    Thread[] navesRebeldes;
    int naveActualAEliminar = 0;

    public CazaImperial(Thread[] navesRebeldes){

        this.navesRebeldes = navesRebeldes;

    }

    @Override
    public void run() {

        Thread.currentThread().setName("Caza Imperial");

        while (naveActualAEliminar < navesRebeldes.length){
            try {
                Thread.sleep(2000);

                if (ThreadLocalRandom.current().nextInt(0,100 ) <= 10 ){
                    System.err.println(Thread.currentThread().getName() + " DISPARA");
                    navesRebeldes[naveActualAEliminar++].interrupt();
                }

            } catch (InterruptedException e) {
                return;
            }
        }

    }
}
