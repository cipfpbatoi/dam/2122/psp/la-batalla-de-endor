package ejercicio4;

import java.util.concurrent.ThreadLocalRandom;

public class EstrellaDeLaMuerte implements Runnable{

    Thread[] navesRebeldes;
    int naveActualAEliminar = 0;

    public EstrellaDeLaMuerte(Thread[] navesRebeldes){

        this.navesRebeldes = navesRebeldes;

    }


    @Override
    public void run() {

        Thread.currentThread().setName("Estrella de la Muerte");

        while (naveActualAEliminar < navesRebeldes.length){
            try {
                Thread.sleep(2000);

                if (ThreadLocalRandom.current().nextInt(0,100 ) <= 70 ){
                    System.err.println(Thread.currentThread().getName() + " DISPARA el Cañón");
                    Thread.State state = navesRebeldes[naveActualAEliminar++].getState();
                    navesRebeldes[naveActualAEliminar++].interrupt();
                }

            } catch (InterruptedException e) {
                System.err.println( "La " + Thread.currentThread().getName() +  " ha sido destruida");
                System.err.println( "LOS ALIADOS GANAN! PAPAPA PA PAPAPA PA ....");
                return;
            }
        }

        System.err.println("El IMPERIO ha aplastado a la escoria Rebelde");

    }
}
