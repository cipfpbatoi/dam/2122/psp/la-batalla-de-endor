import ejercicio2.Escuadron;
import ejercicio2.XWing;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Ejercicio2 {

    public static void main(String[] args) {

        System.out.println("Empezando simulación");

        ExecutorService executorService = Executors.newFixedThreadPool(20);

        Escuadron escuadron = new Escuadron();

        for (int i = 0; i < 10; i++) {

            executorService.execute(new XWing(i+"",escuadron));

        }

        executorService.shutdown();


        try {

            while (!executorService.awaitTermination(10, TimeUnit.SECONDS)){

                System.err.println("SIMULATION_CONTROL: la simulacion no ha finalizando todavia, seguimos a la espera" );

            }

            System.out.println("El escuadron ha acabado con un total de " + escuadron.getNumeroCazasAbatidos() + " cazas");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
